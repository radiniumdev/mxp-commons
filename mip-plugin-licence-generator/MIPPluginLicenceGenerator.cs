﻿using CERTENROLLLib;
using Cygnetic.MIPPluginCommons.Licence;
using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Cygnetic
{


    public class MIPPluginLicenceGenerator
    {
        static string pluginName;
        static string rootCertPath;
        static LicenceType[] licenceTypes = Enum.GetValues(typeof(LicenceType)) as LicenceType[];



        public static X509Certificate2 CreateSelfSignedCertificate(string subjectName)
        {
            // create DN for subject and issuer
            var dn = new CX500DistinguishedName();
            dn.Encode("CN=" + subjectName, X500NameFlags.XCN_CERT_NAME_STR_NONE);

            // create a new private key for the certificate
            CX509PrivateKey privateKey = new CX509PrivateKey();
            privateKey.ProviderName = "Microsoft Enhanced RSA and AES Cryptographic Provider";
            privateKey.MachineContext = true;
            privateKey.Length = 2048;
            privateKey.KeySpec = X509KeySpec.XCN_AT_SIGNATURE; // use is not limited
            privateKey.ExportPolicy = X509PrivateKeyExportFlags.XCN_NCRYPT_ALLOW_PLAINTEXT_EXPORT_FLAG;
            privateKey.Create();

            // Use the stronger SHA512 hashing algorithm
            var hashobj = new CObjectId();
            hashobj.InitializeFromAlgorithmName(ObjectIdGroupId.XCN_CRYPT_HASH_ALG_OID_GROUP_ID,
                ObjectIdPublicKeyFlags.XCN_CRYPT_OID_INFO_PUBKEY_ANY,
                AlgorithmFlags.AlgorithmFlagsNone, "SHA512");

            // add extended key usage if you want - look at MSDN for a list of possible OIDs
            var oid = new CObjectId();
            oid.InitializeFromValue("1.3.6.1.5.5.7.3.1"); // SSL server
            var oidlist = new CObjectIds();
            oidlist.Add(oid);
            var eku = new CX509ExtensionEnhancedKeyUsage();
            eku.InitializeEncode(oidlist);

            // Create the self signing request
            var cert = new CX509CertificateRequestCertificate();
            cert.InitializeFromPrivateKey(X509CertificateEnrollmentContext.ContextMachine, privateKey, "");
            cert.Subject = dn;
            cert.Issuer = dn; // the issuer and the subject are the same
            cert.NotBefore = DateTime.Now;
            // this cert expires immediately. Change to whatever makes sense for you
            cert.NotAfter = DateTime.Now.AddYears(20);
            cert.X509Extensions.Add((CX509Extension)eku); // add the EKU
            cert.HashAlgorithm = hashobj; // Specify the hashing algorithm
            cert.Encode(); // encode the certificate

            // Do the final enrollment process
            var enroll = new CX509Enrollment();
            enroll.InitializeFromRequest(cert); // load the certificate
            enroll.CertificateFriendlyName = subjectName; // Optional: add a friendly name
            string csr = enroll.CreateRequest(); // Output the request in base64
                                                 // and install it back as the response
            enroll.InstallResponse(InstallResponseRestrictionFlags.AllowUntrustedCertificate,
                csr, EncodingType.XCN_CRYPT_STRING_BASE64, ""); // no password
                                                                // output a base64 encoded PKCS#12 so we can import it back to the .Net security classes
            var base64encoded = enroll.CreatePFX("", // no password, this is for internal consumption
                PFXExportOptions.PFXExportChainWithRoot);

            // instantiate the target class with the PKCS#12 data (and the empty password)
            return new System.Security.Cryptography.X509Certificates.X509Certificate2(
                System.Convert.FromBase64String(base64encoded), "",
                // mark the private key as exportable (this is usually what you want to do)
                System.Security.Cryptography.X509Certificates.X509KeyStorageFlags.Exportable
            );
        }

        private static X509Certificate2 LoadOrCreateCertificate(string fileName)
        {
            X509Certificate2 loadedCert = null;

            if (File.Exists(fileName))
            {
                try
                {
                    loadedCert = new System.Security.Cryptography.X509Certificates.X509Certificate2(fileName);
                }
                catch (Exception)
                { }
            }

            if (loadedCert == null)
            {
                loadedCert = CreateSelfSignedCertificate(pluginName);
                byte[] exportedCert = loadedCert.Export(X509ContentType.Pkcs12);
                File.WriteAllBytes(fileName, exportedCert);
            }

            return loadedCert;
        }

        public static void PrintHelp()
        {
            Console.Out.WriteLine("You need to supply <PluginName> <certPath> <Mode> <fingerprint> <Amount> <Days> <licenceType>");
            Console.Out.WriteLine("<Mode> can be 'New', 'Validate' or 'Export'");
            Console.Out.WriteLine("<Fingerprint> is a fingerprint provided by the client consuming a plugin");
            Console.Out.WriteLine("<Amount> is the amount of licences being issued");
            Console.Out.WriteLine("<licenceType> is one of: " + String.Join(", ", licenceTypes));
        }
        /*
         * Takes the root certificate and generates a licence, or validates a licence.
         * Arguments:
         * "NewLicence", serverSLC, numLicences
         * "ValidateLicence", curLicence
         * "ExportRootPublicKey", fileName
         * 
         * If the root cert does not exist, it is created
         */
        public static void Main(string[] args)
        {
            Console.Out.WriteLine(args + " " + args.Count());
            pluginName = args[0];
            rootCertPath = args[1];

            if (pluginName == null)
            {
                throw new System.Exception("Plugin not configured!");
            }


            if (rootCertPath == null)
            {
                throw new System.Exception("RootCertPath not configured!");
            }

            if (args.Length < 4)
            {
                PrintHelp();
                return;
            }

            string mode = args[2];

            X509Certificate2 certificateMain = LoadOrCreateCertificate(rootCertPath);
            if (certificateMain.FriendlyName != pluginName)
            {
                Console.Out.WriteLine("Invalid root cert!");
                return;
            }

            if (mode == "New")
            {
                if (args.Length < 6)
                {
                    PrintHelp();
                    return;
                }
                


                string numLicences = args[4];
                string numDays = args[5];
                string licenceTypeString = args[6];
                LicenceType licenceType = licenceTypes.FirstOrDefault(lt => lt.ToString() == licenceTypeString);


                if (licenceType == null)
                {
                    PrintHelp();
                    return;
                }





                byte[] numLicencesBytes = BitConverter.GetBytes(int.Parse(numLicences));
                byte[] fingerprint = Encoding.ASCII.GetBytes(args[3]);
                byte[] expiryDateBytes = Encoding.ASCII.GetBytes(DateTime.Now.AddDays(int.Parse(numDays)).ToString("s")); //ISO8601 format
                byte[] fingerprintLengthBytes = BitConverter.GetBytes(fingerprint.Length);
                byte[] licenceTypeBytes = BitConverter.GetBytes(licenceType.GetHashCode());

                RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider();

                byte[] randomValue = new byte[28];
                rngCsp.GetBytes(randomValue);

                byte[] dataToHash = fingerprintLengthBytes.Concat(fingerprint).Concat(randomValue).Concat(numLicencesBytes).Concat(expiryDateBytes).Concat(licenceTypeBytes).ToArray();

                RSACryptoServiceProvider prv = certificateMain.PrivateKey as RSACryptoServiceProvider;
                SHA512 sha512 = new SHA512Managed();
                byte[] hashedData = sha512.ComputeHash(dataToHash);

                byte[] signature = prv.SignHash(hashedData, CryptoConfig.MapNameToOID("SHA512"));
                string base64licence = Convert.ToBase64String(dataToHash.Concat(signature).ToArray());
                Base64Licence lic = new Base64Licence(base64licence);
                Console.Out.Write(base64licence);
            }
            else if (mode == "Validate")
            {
                if (args.Length < 4)
                {
                    PrintHelp();
                    return;
                }

                RSACryptoServiceProvider csp = (RSACryptoServiceProvider)certificateMain.PublicKey.Key;
                byte[] fileBytes = Convert.FromBase64String(File.ReadAllText(args[3]));
                int fingerprintLengthBytes = BitConverter.ToInt32(fileBytes.Take(4).ToArray(), 0);
                byte[] licenceBytes = fileBytes.Skip(4).Take(fingerprintLengthBytes).ToArray();
                byte[] randomBytes = fileBytes.Skip(4 + fingerprintLengthBytes).Take(28).ToArray();
                int numLicences = BitConverter.ToInt32(fileBytes.Skip(4 + fingerprintLengthBytes + 28).Take(4).ToArray(), 0);
                byte[] expiryDateBytes = fileBytes.Skip(4 + fingerprintLengthBytes + 28 + 4).Take(19).ToArray();
                byte[] licenseTypeBytes = fileBytes.Skip(4 + fingerprintLengthBytes + 28 + 4 + 19).Take(4).ToArray();
                byte[] hashBytes = fileBytes.Take(4 + fingerprintLengthBytes + 28 + 4 + 19 + 4).ToArray();
                byte[] signatureBytes = fileBytes.Skip(4+fingerprintLengthBytes + 28 + 4 + 19 + 4).ToArray();


                DateTime expiryDate = DateTime.Parse(Encoding.ASCII.GetString(expiryDateBytes));
                RSACryptoServiceProvider prv = certificateMain.PrivateKey as RSACryptoServiceProvider;
                SHA512 sha512 = new SHA512Managed();
                byte[] dataToValidateAfterHash = sha512.ComputeHash(hashBytes);

                bool verified = csp.VerifyHash(dataToValidateAfterHash, CryptoConfig.MapNameToOID("SHA512"), signatureBytes);
                if (!verified)
                {
                    Console.WriteLine("Invalid licence!");
                }
                else
                {
                    Console.WriteLine(numLicences.ToString() + " licences valid");
                    Console.WriteLine("Expires: " + expiryDate);
                }
            }
            else if (mode == "Export")
            {
                if (args.Length < 4)
                {
                    PrintHelp();
                    return;
                }
                File.WriteAllBytes(args[3], certificateMain.Export(X509ContentType.Cert));
            }

        }
    }
}
