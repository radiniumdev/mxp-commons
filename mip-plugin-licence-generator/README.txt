﻿Below are the commands required to generate a new licence signing certificate. (Replace "FirePanelIntegration" with required friendly name)

openssl req -x509 -sha512 -nodes -days 365 -newkey rsa:2048 -keyout FirePanelIntegration.key -out FirePanelIntegration.crt
openssl x509 -outform pem -in FirePanelIntegration.crt -out FirePanelIntegration.pem
openssl pkcs12 -export -CSP "Microsoft Enhanced RSA and AES Cryptographic Provider" -inkey FirePanelIntegration.key  -in FirePanelIntegration.pem -name FirePanelIntegration -out FirePanelIntegration.pkcs12

