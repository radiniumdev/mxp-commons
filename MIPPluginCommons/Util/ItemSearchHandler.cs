﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VideoOS.Platform;

namespace Cygnetic.MIPPluginCommons.Util
{
    public class ItemSearchHandler
    {
        public List<Item> ItemList { get; set; }

        public ItemSearchHandler(List<Item> itemList)
        {
            ItemList = itemList;
        }
        private IEnumerable<Item> FilteredItems(string text)
        {
            return ItemList.Where(i => i.Name.ToLowerInvariant().Contains(text.ToLowerInvariant())).OrderBy(i => i.Name);
        }

        //Written for keyup event
        public void ItemComboSearch(object sender, EventArgs e)
        {
            KeyEventArgs kea = (KeyEventArgs)e;

            if (kea.Alt || kea.Shift || kea.Control || (new Keys[] { Keys.Up, Keys.Down, Keys.Left, Keys.Right, Keys.Tab, Keys.Enter }).Contains(kea.KeyCode))
            {
                return;
            }

            ComboBox combo = (ComboBox)sender;

            var filtered = FilteredItems(combo.Text);
        }
    }
}
