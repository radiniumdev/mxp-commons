﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cygnetic.MIPPluginCommons.Util
{
    class DatabaseUtility
    {
        /*
         * This method only supports database logins and current windows user login
         * 
         */
        public static string SQL_CONNECTION_STRING(string username, string dataSource, string catalogue, string password)
        {

            if (username.Length > 0)
            {//Database login
                return String.Format(@"User Id={0};Password={1};data source={2};initial catalog={3}", new object[] { username, password, dataSource, catalogue });
            }
            else
            {//Windows login
                return String.Format(@"Integrated Security=SSPI;data source={0};initial catalog={1}", new object[] { dataSource, catalogue });
            }
        }
    }
}
