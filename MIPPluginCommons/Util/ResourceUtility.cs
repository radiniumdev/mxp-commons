﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.IconLib;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Cygnetic.MIPPluginCommons.Util
{
    public class ResourceUtility
    {
        private static Icon cygneticIcon;
        private static Image cygneticImage;

        static ResourceUtility()
        {
            Type type = typeof(ResourceUtility);
            cygneticImage = new Bitmap(type.Assembly.GetManifestResourceStream("Cygnetic.MIPPluginCommons.Resources.cygtechlogo.png"));

            cygneticIcon =  ScaleImageToIconSizes(cygneticImage);
        }

        public static Icon CygneticIcon()
        {
            return cygneticIcon;
        }


        public static Bitmap ResizeImage(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(width, height);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }

        public static Image ImageFromAssemblyPath(Type type, string path)
        {


            string assemblyPath = Path.GetDirectoryName(type.Assembly.Location);

            return Image.FromFile(assemblyPath + Path.DirectorySeparatorChar + path);
        }

        public static Image ImageFromAssembly(Type type, string path)
        {
            
            return Image.FromStream(type.Assembly.GetManifestResourceStream(path));
        }

        public static Icon ScaleImageToIconSizes(Image image)
        {
            MultiIcon multiicon = new MultiIcon();
            SingleIcon icon = multiicon.Add("icon");
            icon.Add(ResizeImage(image, 8, 8));
            icon.Add(ResizeImage(image, 16, 16));
            icon.Add(ResizeImage(image, 24, 24));
            icon.Add(ResizeImage(image, 32, 32));
            icon.Add(ResizeImage(image, 48, 48));
            icon.Add(ResizeImage(image, 64, 64));

            return icon.Icon;
        }
    }
}
