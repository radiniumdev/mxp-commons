﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoOS.Platform;

namespace Cygnetic.MIPPluginCommons.Util
{
    public class CameraUtility
    {
        private static List<Item> cameras = null;

        public static List<Item> GetCamerasList()
        {
            if (cameras == null)
            {
                cameras = Configuration.Instance.GetItems().ToList();
                cameras.AddRange(GetCamerasList(cameras));
                //Sorted list
                cameras = cameras.Where(x => x.Enabled && x.FQID.FolderType == FolderType.No && x.FQID.Kind == Kind.Camera).GroupBy(x => x.FQID.ObjectId).Select(x => x.FirstOrDefault()).OrderBy(camera => new Tuple<int, string>(camera.Name.TakeWhile(x => char.IsDigit(x)).ToArray().Length > 0 ? int.Parse(new string(camera.Name.TakeWhile(x => char.IsDigit(x)).ToArray())) : int.MaxValue, camera.Name)).ThenBy(x => x.Name).Distinct().ToList();
            }
            return cameras;
        }

        public static List<Item> GetCamerasList(List<Item> topItems)
        {
            List<Item> resultList = new List<Item>();
            foreach (Item item in topItems)
            {
                resultList.AddRange(GetCamerasList(item).Where(x => x.Enabled && x.FQID.FolderType == FolderType.No && x.FQID.Kind == Kind.Camera));
            }
            return resultList;
        }

        public static List<Item> GetCamerasList(Item topItem)
        {
            List<Item> camerasList = new List<Item>();
            if (topItem != null && topItem.GetChildren() != null)
            {
                foreach (Item item in topItem.GetChildren())
                {
                    if (item.Enabled && item.FQID.FolderType == FolderType.No && item.FQID.Kind == Kind.Camera)
                    {
                        camerasList.Add(item);
                    }

                    if (item.FQID.FolderType != FolderType.No)
                    {
                        camerasList.AddRange(GetCamerasList(item));
                    }
                }
            }

            return camerasList;
        }


    }
}
