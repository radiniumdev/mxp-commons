﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cygnetic.MIPPluginCommons.UI
{
    public partial class CygneticForm : Form
    {
        public CygneticForm()
        {
            InitializeComponent();
            CenterToScreen();
        }

        public CygneticForm(string text) : this()
        {
            Text = text;
        }

        public void Recenter()
        {
            CenterToScreen();
        }
    }
}
