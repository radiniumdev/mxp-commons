﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cygnetic.MIPPluginCommons.Licence
{
    public partial class LicenceSiteApiKeyHelpUserControl : UserControl
    {
        public LicenceSiteApiKeyHelpUserControl()
        {
            InitializeComponent();

            Dock = DockStyle.Fill;

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://portal.radinium.com");
        }
    }
}
