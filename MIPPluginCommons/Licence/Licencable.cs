﻿using Cygnetic.MIPPluginCommons.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cygnetic.MIPPluginCommons.Licence
{
    public class Licencable
    {


        public Licencable(string name, params object[] parts)
        {
            Name = name;
            FingerprintParts = parts;

        }

        public object[] FingerprintParts
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public Base64Licence Licence { get; set; }

        public LicensableState State{ get; set;}

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            if (Licence != null)
            {
                sb.Append("(");
                sb.Append(Licence.LicenceType.ToString());
                if(Licence.LicenceType == LicenceType.Grace)
                {
                    if (Licence.ExpiryDateDateTime < DateTime.Now)
                    {
                        sb.Append(": Expired on ");
                    }
                    else
                    {
                        sb.Append(": Expires on ");
                    }
                    sb.Append(Licence.ExpiryDateDateTime.ToString("yyyy-MM-dd"));
                }
                sb.Append(") ");
            }

            sb.Append(Name);

            if(Licence != null && Licence.NumLicences > 1)
            {
                sb.Append(" - " + Licence.NumLicences + " Licences total");
            }

            return sb.ToString();
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (!(obj is Licencable)) return false;

            Licencable l = obj as Licencable;

            if (l.FingerprintParts.Length != FingerprintParts.Length) return false;
            
            for(int i = 0; i < FingerprintParts.Length; i++)
            {
                if (!l.FingerprintParts[i].Equals(FingerprintParts[i])) return false;
                
            }

            return true;
        }

        public override int GetHashCode()
        {
            int hash = 17;
            for(int i = 0; i < FingerprintParts.Length; i++)
            {
                hash += 31 * FingerprintParts[i].GetHashCode();
            }

            return hash;
        }

    }

    public enum LicensableState
    {
        Unlicensed,
        Licensed
    }

}
