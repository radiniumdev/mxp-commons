﻿using System;
using System.Linq;
using System.Text;

namespace Cygnetic.MIPPluginCommons.Licence
{
    public class Base64Licence
    {
        public byte[] Licence { get; set; }

        public byte[] Signature
        {
            get { return Licence.Skip(DataStartIdx + 27).ToArray(); }
        }

        public byte[] DataHash
        {
            get { return Licence.Take(DataStartIdx + 27).ToArray(); }
        }

        public byte[] Fingerprint 
        {
            get
            {
                return Licence.Skip(4).Take(FingerprintLength).ToArray();
            }
        }

        public int FingerprintLength
        {
            get
            {
                return BitConverter.ToInt32(Licence.Take(4).ToArray(), 0);
            }
        }

        public string FingerprintAsString
        {
            get
            {
                return Encoding.ASCII.GetString(Fingerprint);
            }
        }

        public int NumLicences 
        {
            get
            {
                return BitConverter.ToInt32(Licence, DataStartIdx);
            }
        }
        public byte[] ExpiryDate 
        {
            get
            {
                return Licence.Skip(DataStartIdx + 4).Take(19).ToArray(); //ISO 8601
            }
        }

        public DateTime ExpiryDateDateTime
        {
            get
            {
                return DateTime.Parse(Encoding.ASCII.GetString(ExpiryDate));
            }
        }

        public LicenceType LicenceType
        {
            get
            {
                return (LicenceType)BitConverter.ToInt32(Licence, DataStartIdx + 23);
            }
        }

        public Base64Licence(string base64licence)
        {
            Licence = Convert.FromBase64String(base64licence);
        }

        public Base64Licence(byte[] licence)
        {
            Licence = licence;
        }

        public override string ToString()
        {
            return Convert.ToBase64String(Licence);
        }

        public int DataStartIdx
        {
            get
            {
                return 4 + Fingerprint.Length + 28; //first 32bit int, fingerprint and 28 random bytes
            }
        }
    }
}
