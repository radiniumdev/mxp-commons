﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cygnetic.MIPPluginCommons.Licence
{
    public class InvalidLicenceException : Exception
    {
        public InvalidLicenceException() : base("Invalid Plugin Licence")
        {

        }

        public InvalidLicenceException(string msg) : base(msg)
        {

        }
    }
}
