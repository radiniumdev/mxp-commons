﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace Cygnetic.MIPPluginCommons.Licence
{
    public class LicenceClient
    {
        public Uri LicenceServerAddress { get; set; }
        private HttpClient client;

        public LicenceClient(Uri licenceServerAddress)
        {
            LicenceServerAddress = licenceServerAddress;
            client = new HttpClient();

            client.BaseAddress = LicenceServerAddress;
        }

        /***
         * Will accept a number of licence request codes(LCR). Each plugin handles how its LCRs are constructed.
         * 
         * The Licence persisted on the server that corresponds to the LCR will be returned.
         * 
         */
        public Base64Licence[] GetLicences(string[] licenceRequestCodes, string apiKey, string licenceRequestType)
        {

            MemoryStream ms = new MemoryStream();

            for(int i =0; i < licenceRequestCodes.Length; i++) 
            {
                string lrc = licenceRequestCodes[i];

                ms.Write(Encoding.ASCII.GetBytes(lrc), 0, lrc.Length);
                if (i < licenceRequestCodes.Length - 1)
                {
                    byte[] newline = Encoding.ASCII.GetBytes(Environment.NewLine);
                    ms.Write(newline, 0, newline.Length);
                }
            }

            ms.Seek(0, SeekOrigin.Begin);

            MultipartFormDataContent formData = new MultipartFormDataContent(DateTime.Now.Ticks.ToString("x")); //we expect to never see this boundary

            formData.Add(new StreamContent(ms), "file", "lrcs.txt");
            formData.Add(new StringContent(apiKey), "api_key");
            formData.Add(new StringContent(licenceRequestType), "licence_request_type");


            //Make request
            HttpResponseMessage response = client.PostAsync("lrc_file", formData).Result; //Blocking

            //Handle response
            JArray responseJson = JArray.Parse(response.Content.ReadAsStringAsync().Result);

            return responseJson.Select(t => new Base64Licence(t.ToString())).ToArray();
        }
        
    }
}
