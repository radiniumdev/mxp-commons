﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VideoOS.Platform;
using System.IO;
using Newtonsoft.Json.Linq;
using Cygnetic.MIPPluginCommons.Util;
using Cygnetic.MIPPluginCommons.UI;

namespace Cygnetic.MIPPluginCommons.Licence
{
    public enum EnumLicensingType
    {
        Site,
        PerItem,
        SiteWithItemCount
    }

    public partial class LicensingUserControl : UserControl
    {
        private EnumLicensingType LicensingType = EnumLicensingType.PerItem;

        private CygneticForm siteHelpForm;

        public LicenceValidator LicenceValidator
        {
            get;
            set;
        }

        public string ApiKey
        {
            get
            {
                return textBoxApiKey.Text;
            }

            set
            {
                textBoxApiKey.Text = value;
            }
        }

        public string LicenceRequestType { get; set; }

        public event EventHandler SaveLicenceSettings;



        public LicensingUserControl() : base()
        {
            InitializeComponent();
            Licencables = new List<Licencable>();
            Licensed = new List<Licencable>();
            Unlicensed = new List<Licencable>();
        }

        public LicensingUserControl(LicenceValidator licenceValidator) : this()
        {
            LicenceValidator = licenceValidator;
            Licencables = new List<Licencable>();
        }

        public LicensingUserControl(LicenceValidator licenceValidator, EnumLicensingType type) : this()
        {
            LicensingType = type;
            LicenceValidator = licenceValidator;
            Licencables = new List<Licencable>();
        }

        public List<Licencable> Unlicensed
        {
            get;
        }

        public List<Licencable> Licensed
        {
            get;
        }

        public List<Licencable> Licencables
        {
            set; get;

        }

        public void addLicensed(Licencable licenceable)
        {
            Licensed.Add(licenceable);
            listBoxLicensed.Items.Add(licenceable);
        }

        public void addUnlicensed(Licencable licenceable)
        {
            Unlicensed.Add(licenceable);
            listBoxUnlicensed.Items.Add(licenceable);
        }

        public new void Update()
        {
            Unlicensed.Clear();
            Licensed.Clear();

            listBoxLicensed.Items.Clear();
            listBoxUnlicensed.Items.Clear();

            if (Licencables != null)
            {
                foreach(Licencable licencable in Licencables)
                {
                    if (LicenceValidator.ContainsLicence(licencable.FingerprintParts))
                    {
                        licencable.Licence = LicenceValidator.LoadLicence(licencable.FingerprintParts);
                        if (licencable.Licence == null)
                        {
                            addLicensed(licencable);
                            return;
                        }
                        if (licencable.Licence.LicenceType == LicenceType.Grace && licencable.Licence.ExpiryDateDateTime < DateTime.Now)
                        {
                            addUnlicensed(licencable);
                        }
                        else
                        {
                            addLicensed(licencable);
                        }
                    }
                    else
                    {
                        addUnlicensed(licencable);
                    }
                }
            }
        }

        private void LRCOnline_Click(object sender, EventArgs evt)
        {

            if(LicenceRequestType == null)
            {
                EnvironmentManager.Instance.Log(true, "Licencing", "LicenceRequestType not configured!");
                return;
            }

            Guid outGuid = new Guid();
            if(textBoxApiKey.Text.Length < 16 || !Guid.TryParse(textBoxApiKey.Text, out outGuid))
            {
                MessageBox.Show("Please check your site key!  It seems incorrect.");
                return;
            }

            string[] fingerprintsAll = SelectedUnlicencedFingerprints();
            string[] fingerprints = fingerprintsAll.Where(x => !String.IsNullOrWhiteSpace(x)).ToArray();

            int unavailable = fingerprintsAll.Count(x => String.IsNullOrWhiteSpace(x));
            if (fingerprints.Length == 0)
            {
                MessageBox.Show("No items available for licensing. " + unavailable + " items are unavailable, check that they are connected and the network online.");
                return;
            }
            try
            {
                LicenceClient client = new LicenceClient(new Uri("https://portal.radinium.com/licence/"));

                Base64Licence[] licences = client.GetLicences(fingerprints, ApiKey, LicenceRequestType);

                int graceLicenceCount = 0;
                int softwareLicenceCount = 0;
                int supportLicence = 0;

                foreach (Base64Licence licence in licences)
                {
                    LicenceValidator.SaveLicence(licence);
                    switch (licence.LicenceType)
                    {
                        case LicenceType.Grace:
                            graceLicenceCount++;
                            break;
                        case LicenceType.Software:
                            softwareLicenceCount++;
                            break;
                        case LicenceType.Support:
                            supportLicence++;
                            break;
                    }
                }
                string totalString = "Licences updated: ";
                string graceLicences = "";
                string softwareLicences = "";
                string unavailableLicences = "";
                if(graceLicenceCount > 0)
                {
                    graceLicences = "" + graceLicenceCount + " grace licences";
                }
                if(softwareLicenceCount > 0)
                {
                    softwareLicences = "" + softwareLicenceCount + " software licences";
                }
                if(unavailable > 0)
                {
                    unavailableLicences = "" + unavailable + " unavailable licences.";
                }
                if(graceLicenceCount <= 0 && softwareLicenceCount <= 0 && unavailable <= 0)
                {
                    totalString += "None";
                }
                else
                {
                    totalString += graceLicences;
                    if(graceLicenceCount > 0)
                    {
                        totalString += " and ";
                    }
                    totalString += softwareLicences + " retrieved. ";
                    totalString += unavailableLicences;
                }
                MessageBox.Show(totalString);

                Update();

                
            }
            catch (Exception e)
            {
                MessageBox.Show("Licence was not obtainable at present, please try again later.");
            }
        }

        private void LRCOffline_Click(object sender, EventArgs e)
        {
            if (LicenceRequestType == null)
            {
                EnvironmentManager.Instance.Log(true, "Licencing", "LicenceRequestType not configured!");
                return;
            }

            string[] fingerprintsAll = SelectedUnlicencedFingerprints();
            string[] fingerprints = fingerprintsAll.Where(x => !String.IsNullOrWhiteSpace(x)).ToArray();

            int unavailable = fingerprintsAll.Count(x => String.IsNullOrWhiteSpace(x));

            string lrc = String.Join(Environment.NewLine, fingerprints);
            DialogResult result = lrcFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                FileStream fs = File.Create(lrcFileDialog.FileName);
                fs.Write(Encoding.ASCII.GetBytes(lrc), 0, lrc.Length);
                fs.Close();
            }

            if(fingerprints.Count() <= 0)
            {
                MessageBox.Show("Unable to generate license request as there are no selected available devices! " + unavailable + " devices are unavailable on the network.  Please check that they are plugged in and online.");
            }
        }

        private string[] UnlicencedFingerprints()
        {
            return Unlicensed.Select(l => LicenceValidator.GenerateFingerprint(l.FingerprintParts)).Where(l => l !=null).ToArray();
        }

        private string[] SelectedUnlicencedFingerprints()
        {
            bool unableLicence = false;
            string[] fingerprints = new string[0];
            if (listBoxUnlicensed.SelectedItems.Count <= 0 && listBoxLicensed.SelectedItems.Count <= 0)
            {
                fingerprints = Licensed.Select(l => LicenceValidator.GenerateFingerprint(l.FingerprintParts)).Where(x => x != null).Union(Unlicensed.Select(l => LicenceValidator.GenerateFingerprint(l.FingerprintParts)).Where(x => x != null)).ToArray();
                unableLicence = fingerprints.Length < (listBoxUnlicensed.Items.Count + listBoxLicensed.Items.Count);
            }
            else
            {
                fingerprints = Licensed.Where(l => listBoxLicensed.SelectedItems.Contains(l)).Select(l => LicenceValidator.GenerateFingerprint(l.FingerprintParts)).Where(l => l != null).Union(Unlicensed.Where(l => listBoxUnlicensed.SelectedItems.Contains(l)).Select(l => LicenceValidator.GenerateFingerprint(l.FingerprintParts)).Where(l => l != null)).ToArray();
                unableLicence = fingerprints.Length < (listBoxLicensed.SelectedItems.Count + listBoxUnlicensed.SelectedItems.Count);
            }
            if(unableLicence)
            {
                MessageBox.Show("Please note that 1 or more items were unable to be licensed. Check that they are configured correctly and are online.");
            }

            return fingerprints; 
        }

        private void LoadLicences_Click(object sender, EventArgs e)
        {
            DialogResult result = licenceFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                byte[] bytes = File.ReadAllBytes(licenceFileDialog.FileName);

                string licencesJsonString = Encoding.ASCII.GetString(bytes);
                try
                {
                    JArray licences = JArray.Parse(licencesJsonString);


                    foreach (string licence in licences)
                    {
                        LicenceValidator.SaveLicence(licence);
                    }

                    Update(); //Update UI
                }
                catch(Exception)
                {
                    MessageBox.Show("Invalid license file", "Error");
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            SaveLicenceSettings?.Invoke(sender, e);
            MessageBox.Show("Saved site key", "Saved");
        }

        private void listBoxUnlicensed_Click(object sender, EventArgs e)
        {
        }

        private void siteApiHelpBox_Click(object sender, EventArgs e)
        {

            if (siteHelpForm == null || siteHelpForm.IsDisposed)
            {
                siteHelpForm = new CygneticForm("Site API Key Instruction");
                siteHelpForm.Controls.Add(new LicenceSiteApiKeyHelpUserControl());
                siteHelpForm.Width = 350;
                siteHelpForm.Height = 120;
                siteHelpForm.Show();
                siteHelpForm.Recenter();
            }
            else
            {
                siteHelpForm.Activate();
                siteHelpForm.Recenter();
            }
            
        }
    }
}
