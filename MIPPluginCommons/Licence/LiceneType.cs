﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cygnetic.MIPPluginCommons.Licence
{
    public enum LicenceType
    {
        Unavailable = -1,
        Grace = 0,
        Software = 1,
        Support = 2

    }
}
