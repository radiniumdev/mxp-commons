﻿using Cygnetic.MIPPluginCommons.Util;
using System.Windows.Forms;
using VideoOS.Platform;

namespace Cygnetic.MIPPluginCommons.Licence
{
    partial class LicensingUserControl : UserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LicensingUserControl));
            this.listBoxUnlicensed = new System.Windows.Forms.ListBox();
            this.listBoxLicensed = new System.Windows.Forms.ListBox();
            this.labelUnlicensed = new System.Windows.Forms.Label();
            this.labelLicensed = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.lrcFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.licenceFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxApiKey = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.siteApiHelpBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.siteApiHelpBox)).BeginInit();
            this.SuspendLayout();
            // 
            // listBoxUnlicensed
            // 
            this.listBoxUnlicensed.FormattingEnabled = true;
            this.listBoxUnlicensed.HorizontalScrollbar = true;
            this.listBoxUnlicensed.Location = new System.Drawing.Point(15, 182);
            this.listBoxUnlicensed.Name = "listBoxUnlicensed";
            this.listBoxUnlicensed.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.listBoxUnlicensed.Size = new System.Drawing.Size(443, 134);
            this.listBoxUnlicensed.TabIndex = 0;
            this.toolTip1.SetToolTip(this.listBoxUnlicensed, "Your unlicenced products.");
            this.listBoxUnlicensed.Click += new System.EventHandler(this.listBoxUnlicensed_Click);
            // 
            // listBoxLicensed
            // 
            this.listBoxLicensed.FormattingEnabled = true;
            this.listBoxLicensed.HorizontalScrollbar = true;
            this.listBoxLicensed.Location = new System.Drawing.Point(15, 55);
            this.listBoxLicensed.Name = "listBoxLicensed";
            this.listBoxLicensed.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.listBoxLicensed.Size = new System.Drawing.Size(443, 108);
            this.listBoxLicensed.TabIndex = 1;
            this.toolTip1.SetToolTip(this.listBoxLicensed, "Your licenced products.");
            this.listBoxLicensed.Click += new System.EventHandler(this.listBoxUnlicensed_Click);
            // 
            // labelUnlicensed
            // 
            this.labelUnlicensed.AutoSize = true;
            this.labelUnlicensed.Location = new System.Drawing.Point(15, 166);
            this.labelUnlicensed.Name = "labelUnlicensed";
            this.labelUnlicensed.Size = new System.Drawing.Size(60, 13);
            this.labelUnlicensed.TabIndex = 2;
            this.labelUnlicensed.Text = "Unlicensed";
            this.toolTip1.SetToolTip(this.labelUnlicensed, "Your unlicenced products.");
            // 
            // labelLicensed
            // 
            this.labelLicensed.AutoSize = true;
            this.labelLicensed.Location = new System.Drawing.Point(15, 36);
            this.labelLicensed.Name = "labelLicensed";
            this.labelLicensed.Size = new System.Drawing.Size(50, 13);
            this.labelLicensed.TabIndex = 3;
            this.labelLicensed.Text = "Licensed";
            this.toolTip1.SetToolTip(this.labelLicensed, "Your licenced products.");
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(263, 322);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(195, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Save Licence Request (Offline)";
            this.toolTip1.SetToolTip(this.button1, "Do a manual offline licence retrieval.");
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.LRCOffline_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(15, 322);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(201, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "Retrieve Licences (Online)";
            this.toolTip1.SetToolTip(this.button2, "Do an automatic online licence retrieval (requires network connectivity).");
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.LRCOnline_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(263, 351);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(195, 23);
            this.button3.TabIndex = 6;
            this.button3.Text = "Load Licences File";
            this.toolTip1.SetToolTip(this.button3, "Load your .lic file containing your product licences.");
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.LoadLicences_Click);
            // 
            // lrcFileDialog
            // 
            this.lrcFileDialog.FileName = "licence-request-code.lrc";
            // 
            // licenceFileDialog
            // 
            this.licenceFileDialog.FileName = "licences.lic";
            this.licenceFileDialog.Filter = "Licence Files|*.lic";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Site key:";
            this.toolTip1.SetToolTip(this.label1, "Your site key obtained for Cygnetic Technologies web portal (http://portal.cygnet" +
        "ictechnologies.com)");
            // 
            // textBoxApiKey
            // 
            this.textBoxApiKey.Location = new System.Drawing.Point(72, 10);
            this.textBoxApiKey.Name = "textBoxApiKey";
            this.textBoxApiKey.Size = new System.Drawing.Size(235, 20);
            this.textBoxApiKey.TabIndex = 8;
            this.toolTip1.SetToolTip(this.textBoxApiKey, "Your site key obtained for Radinium web portal (https://portal.radinium.com)");
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(345, 10);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 9;
            this.button4.Text = "Save";
            this.toolTip1.SetToolTip(this.button4, "Save current portal settings.");
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // siteApiHelpBox
            // 
            this.siteApiHelpBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.siteApiHelpBox.Image = ((System.Drawing.Image)(resources.GetObject("siteApiHelpBox.Image")));
            this.siteApiHelpBox.Location = new System.Drawing.Point(319, 10);
            this.siteApiHelpBox.Name = "siteApiHelpBox";
            this.siteApiHelpBox.Size = new System.Drawing.Size(20, 20);
            this.siteApiHelpBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.siteApiHelpBox.TabIndex = 10;
            this.siteApiHelpBox.TabStop = false;
            this.siteApiHelpBox.Click += new System.EventHandler(this.siteApiHelpBox_Click);
            // 
            // LicensingUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.siteApiHelpBox);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.textBoxApiKey);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.labelLicensed);
            this.Controls.Add(this.labelUnlicensed);
            this.Controls.Add(this.listBoxLicensed);
            this.Controls.Add(this.listBoxUnlicensed);
            this.Name = "LicensingUserControl";
            this.Size = new System.Drawing.Size(461, 403);
            ((System.ComponentModel.ISupportInitialize)(this.siteApiHelpBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxUnlicensed;
        private System.Windows.Forms.ListBox listBoxLicensed;
        private System.Windows.Forms.Label labelUnlicensed;
        private System.Windows.Forms.Label labelLicensed;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private SaveFileDialog lrcFileDialog;
        private OpenFileDialog licenceFileDialog;
        private Label label1;
        private TextBox textBoxApiKey;
        private Button button4;
        private ToolTip toolTip1;
        private PictureBox siteApiHelpBox;
    }
}
