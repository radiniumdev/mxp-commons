﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.Http;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VideoOS.Platform;

namespace Cygnetic.MIPPluginCommons.Licence
{
    public abstract class FingerprintUtil
    {
        public abstract string GenerateFingerprint(object[] parts);
        public FingerprintUtil()
        {
        }

        public bool MatchFingerprint(string base64Fingerprint, object[] parts)
        {
            if (base64Fingerprint == null) return false;

            string fingerprint = TryGenerateFingerprint(parts);
            return base64Fingerprint.Equals(fingerprint);
        }

        public string TryGenerateFingerprint(params object[] parts)
        {
            try
            {
               return GenerateFingerprint(parts);
            }
            catch (Exception e) { }
            return null;
        }

        public bool MatchFingerprint(byte[] base64Fingerprint, object[] parts)
        {
            return MatchFingerprint(Encoding.ASCII.GetString(base64Fingerprint), parts);
        }

        protected string Base64(string plain)
        {
            byte[] bytes = Encoding.ASCII.GetBytes(plain);
            return Convert.ToBase64String(bytes);
        }

        protected static string Unbase64(string base64)
        {
            return Encoding.ASCII.GetString(Convert.FromBase64String(base64));
        }


        //Return a hardware identifier
        protected string Identifier(string wmiClass, string wmiProperty, string wmiMustBeTrue)
        {
            string result = "";
            ManagementClass mc = new ManagementClass(wmiClass);
            ManagementObjectCollection moc = mc.GetInstances();
            foreach (ManagementBaseObject mo in moc)
            {
                if (mo[wmiMustBeTrue].ToString() != "True") continue;
                //Only get the first one
                if (result != "") continue;
                try
                {
                    result = mo[wmiProperty].ToString();
                    break;
                }
                catch
                {
                }
            }
            return result;
        }
        //Return a hardware identifier
        protected string Identifier(string wmiClass, string wmiProperty)
        {
            string result = "";
            ManagementClass mc = new ManagementClass(wmiClass);
            ManagementObjectCollection moc = mc.GetInstances();
            foreach (ManagementBaseObject mo in moc)
            {
                //Only get the first one
                if (result != "") continue;
                try
                {
                    result = mo[wmiProperty].ToString();
                    break;
                }
                catch
                {
                }
            }
            return result;
        }
        protected string CpuId()
        {
            //Uses first CPU identifier available in order of preference
            //Don't get all identifiers, as it is very time consuming
            string retVal = Identifier("Win32_Processor", "UniqueId");
            if (retVal != "") return retVal;
            retVal = Identifier("Win32_Processor", "ProcessorId");
            if (retVal != "") return retVal;
            retVal = Identifier("Win32_Processor", "Name");
            if (retVal == "") //If no Name, use Manufacturer
            {
                retVal = Identifier("Win32_Processor", "Manufacturer");
            }
            //Add clock speed for extra security
            retVal += Identifier("Win32_Processor", "MaxClockSpeed");
            return retVal;
        }
        //BIOS Identifier
        protected string BiosId()
        {
            return Identifier("Win32_BIOS", "Manufacturer") + Identifier("Win32_BIOS", "SMBIOSBIOSVersion") + Identifier("Win32_BIOS", "IdentificationCode") + Identifier("Win32_BIOS", "SerialNumber") + Identifier("Win32_BIOS", "ReleaseDate") + Identifier("Win32_BIOS", "Version");
        }
        //Main physical hard drive ID
        protected string DiskId()
        {
            return Identifier("Win32_DiskDrive", "Model") + Identifier("Win32_DiskDrive", "Manufacturer") + Identifier("Win32_DiskDrive", "Signature") + Identifier("Win32_DiskDrive", "TotalHeads");
        }
        //Motherboard ID
        protected string MotherboardId()
        {
            return Identifier("Win32_BaseBoard", "Model") + Identifier("Win32_BaseBoard", "Manufacturer") + Identifier("Win32_BaseBoard", "Name") + Identifier("Win32_BaseBoard", "SerialNumber");
        }
        //Primary video controller ID
        protected string VideoId()
        {
            return Identifier("Win32_VideoController", "DriverVersion") + Identifier("Win32_VideoController", "Name");
        }
        //First enabled network card ID
        protected string MacId()
        {
            return Identifier("Win32_NetworkAdapterConfiguration", "MACAddress", "IPEnabled");
        }

        protected string MilestoneSLC()
        {
            return EnvironmentManager.Instance.SystemLicense.SLC;
            /*
            byte[] hashBytes = ASCIIEncoding.ASCII.GetBytes(EnvironmentManager.Instance.SystemLicense.SLC);
            SHA512 sha512 = new SHA512Managed();
            return ASCIIEncoding.ASCII.GetString(sha512.ComputeHash(hashBytes).Take(32).ToArray());
            */
        }
        
        [DllImport("iphlpapi.dll", ExactSpelling = true)]
        public static extern int SendARP(uint destIP, uint srcIP, byte[] macAddress, ref uint macAddressLength);

        private byte[] GetMACFromHTTPRequest(IPAddress address)
        {
            HttpClient client = new HttpClient();
            client.Timeout = TimeSpan.FromSeconds(3);
            string callUrl = "http://" + address.ToString() + "/mac.htm";
            var responseWaiting = client.GetAsync(callUrl);
            responseWaiting.Wait();
            HttpResponseMessage message = responseWaiting.Result;
            var responseStr = message.Content.ReadAsStringAsync();
            responseStr.Wait();
            string mac = responseStr.Result;
            return PhysicalAddress.Parse(mac.Replace(":", "-")).GetAddressBytes();
        }

        protected byte[] GetMacAddress(IPAddress address)
        {
            byte[] mac = new byte[6];
            
            Ping pingSender = new Ping();
            string data = "a";
            byte[] buffer = Encoding.ASCII.GetBytes(data);

            // Wait 2 seconds for a reply.
            int timeout = 2000;

            PingOptions options = new PingOptions(128, true);

            // Send the request.
            PingReply reply = pingSender.Send(address, timeout, buffer, options);

            if(reply.Status != IPStatus.Success)
            {
                throw new Exception("Unable to ping remote device.");
            }
            uint len = (uint)mac.Length;
            byte[] addressBytes = address.GetAddressBytes();
            uint dest = ((uint)addressBytes[3] << 24)
              + ((uint)addressBytes[2] << 16)
              + ((uint)addressBytes[1] << 8)
              + ((uint)addressBytes[0]);
            if (SendARP(dest, 0, mac, ref len) != 0 || mac.Count() < 6)
            {
                byte[] httpMac = GetMACFromHTTPRequest(address);
                if (httpMac.Count() == 0)
                    throw new Exception("The HTTP request failed.");
                mac = httpMac;
            }
            
            return mac;
        }

        public string GetMacAddressAsString(IPAddress address)
        {
            return string.Join(":", (from z in GetMacAddress(address) select z.ToString("X2")).ToArray());
        }

        public string GetJetsonSerial(Item jetson)
        {
            try
            {


                ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
                dynamic camConfig = JsonConvert.DeserializeObject(jetson.Properties["camera_config"]);
                string host = camConfig.serverHost;
                string password = camConfig.authentication;

                CookieContainer cookies = new CookieContainer();
                HttpClientHandler handler = new HttpClientHandler();
                handler.CookieContainer = cookies;
                handler.UseCookies = true;
                handler.CookieContainer.Add(new Cookie("testing", "1", "/", host));
                handler.CookieContainer.Add(new Cookie("redirect", "1", "/", host));
                HttpClient httpClient = new HttpClient(handler);
                httpClient.Timeout = TimeSpan.FromSeconds(3);
                Dictionary<string, string> formContentDict = new Dictionary<string, string>();
                formContentDict["user"] = "radinium";
                formContentDict["pass"] = password;
                FormUrlEncodedContent formcontent = new FormUrlEncodedContent(formContentDict);
                var response = httpClient.PostAsync("https://" + host + ":10000/session_login.cgi", formcontent).Result;
                httpClient.Dispose();

                handler = new HttpClientHandler();
                handler.CookieContainer = cookies;
                handler.UseCookies = true;
                httpClient = new HttpClient(handler);
                httpClient.Timeout = TimeSpan.FromSeconds(3);
                var result = httpClient.GetAsync("https://" + host + ":10000/configfile/get_serial.cgi").Result;
                var resultText = result.Content.ReadAsStringAsync().Result;
                var substr = resultText.Substring(resultText.LastIndexOf('>') + 2, resultText.Length - resultText.LastIndexOf('>') - 3);

                ServicePointManager.ServerCertificateValidationCallback = null;
                return substr;
            }

            catch (Exception ex)
            {
                ServicePointManager.ServerCertificateValidationCallback = null;
                return null;
            }
        }

    }
}
