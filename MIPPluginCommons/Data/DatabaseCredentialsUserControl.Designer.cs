﻿namespace Cygnetic.MIPPluginCommons.Data
{
    partial class DatabaseCredentialsUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonTest = new System.Windows.Forms.Button();
            this.labelDBSchema = new System.Windows.Forms.Label();
            this.labelDBDataSource = new System.Windows.Forms.Label();
            this.textBoxDBCatalogue = new System.Windows.Forms.TextBox();
            this.textBoxDBDataSource = new System.Windows.Forms.TextBox();
            this.textBoxDBPassword = new System.Windows.Forms.TextBox();
            this.textBoxDBUsername = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.progressBarTestStatus = new System.Windows.Forms.ProgressBar();
            this.textBoxStatusResult = new System.Windows.Forms.TextBox();
            this.labelTestStatus = new System.Windows.Forms.Label();
            this.checkBoxTestStatus = new System.Windows.Forms.CheckBox();
            this.timerProgressBar = new System.Windows.Forms.Timer(this.components);
            this.toolTipDb = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // buttonTest
            // 
            this.buttonTest.Location = new System.Drawing.Point(12, 130);
            this.buttonTest.Name = "buttonTest";
            this.buttonTest.Size = new System.Drawing.Size(101, 23);
            this.buttonTest.TabIndex = 0;
            this.buttonTest.Text = "Test Connection";
            this.buttonTest.UseVisualStyleBackColor = true;
            this.buttonTest.Click += new System.EventHandler(this.testConnection_Click);
            // 
            // labelDBSchema
            // 
            this.labelDBSchema.AutoSize = true;
            this.labelDBSchema.Location = new System.Drawing.Point(182, 88);
            this.labelDBSchema.Name = "labelDBSchema";
            this.labelDBSchema.Size = new System.Drawing.Size(58, 13);
            this.labelDBSchema.TabIndex = 36;
            this.labelDBSchema.Text = "Catalogue:";
            // 
            // labelDBDataSource
            // 
            this.labelDBDataSource.AutoSize = true;
            this.labelDBDataSource.Location = new System.Drawing.Point(9, 88);
            this.labelDBDataSource.Name = "labelDBDataSource";
            this.labelDBDataSource.Size = new System.Drawing.Size(109, 13);
            this.labelDBDataSource.TabIndex = 35;
            this.labelDBDataSource.Text = "Engine (DataSource):";
            // 
            // textBoxDBCatalogue
            // 
            this.textBoxDBCatalogue.Location = new System.Drawing.Point(180, 104);
            this.textBoxDBCatalogue.Name = "textBoxDBCatalogue";
            this.textBoxDBCatalogue.Size = new System.Drawing.Size(160, 20);
            this.textBoxDBCatalogue.TabIndex = 34;
            this.toolTipDb.SetToolTip(this.textBoxDBCatalogue, "The database or catalogue in Microsoft SQL Server");
            // 
            // textBoxDBDataSource
            // 
            this.textBoxDBDataSource.Location = new System.Drawing.Point(12, 104);
            this.textBoxDBDataSource.Name = "textBoxDBDataSource";
            this.textBoxDBDataSource.Size = new System.Drawing.Size(160, 20);
            this.textBoxDBDataSource.TabIndex = 33;
            this.toolTipDb.SetToolTip(this.textBoxDBDataSource, "The database engine.\r\nUsually this is the hostname of the SQL machine,\r\nor in the" +
        " format [host]\\[instance]\r\nEXAMPLES:\r\nSQL.CCTV.LOCAL\\SQLEXPRESS\r\nSQL.CCTV.LOCAL\r" +
        "\n192.168.0.2");
            // 
            // textBoxDBPassword
            // 
            this.textBoxDBPassword.Location = new System.Drawing.Point(12, 65);
            this.textBoxDBPassword.Name = "textBoxDBPassword";
            this.textBoxDBPassword.PasswordChar = '•';
            this.textBoxDBPassword.Size = new System.Drawing.Size(328, 20);
            this.textBoxDBPassword.TabIndex = 32;
            this.toolTipDb.SetToolTip(this.textBoxDBPassword, "The password if a SQL user is used");
            // 
            // textBoxDBUsername
            // 
            this.textBoxDBUsername.Location = new System.Drawing.Point(12, 26);
            this.textBoxDBUsername.Name = "textBoxDBUsername";
            this.textBoxDBUsername.Size = new System.Drawing.Size(328, 20);
            this.textBoxDBUsername.TabIndex = 31;
            this.toolTipDb.SetToolTip(this.textBoxDBUsername, "The username if SQL login is used\r\nIf the service Windows user is used,\r\nthis sho" +
        "uld be blank");
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 49);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(232, 13);
            this.label12.TabIndex = 30;
            this.label12.Text = "Password (If using Windows login, leave blank):";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 10);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(286, 13);
            this.label11.TabIndex = 29;
            this.label11.Text = "SQL Login Username (if using Windows login, leave blank):";
            // 
            // progressBarTestStatus
            // 
            this.progressBarTestStatus.Location = new System.Drawing.Point(218, 130);
            this.progressBarTestStatus.Name = "progressBarTestStatus";
            this.progressBarTestStatus.Size = new System.Drawing.Size(122, 23);
            this.progressBarTestStatus.TabIndex = 37;
            // 
            // textBoxStatusResult
            // 
            this.textBoxStatusResult.Enabled = false;
            this.textBoxStatusResult.Location = new System.Drawing.Point(12, 159);
            this.textBoxStatusResult.Multiline = true;
            this.textBoxStatusResult.Name = "textBoxStatusResult";
            this.textBoxStatusResult.ReadOnly = true;
            this.textBoxStatusResult.Size = new System.Drawing.Size(328, 108);
            this.textBoxStatusResult.TabIndex = 38;
            this.textBoxStatusResult.TabStop = false;
            this.textBoxStatusResult.Visible = false;
            // 
            // labelTestStatus
            // 
            this.labelTestStatus.AutoSize = true;
            this.labelTestStatus.Location = new System.Drawing.Point(151, 135);
            this.labelTestStatus.Name = "labelTestStatus";
            this.labelTestStatus.Size = new System.Drawing.Size(61, 13);
            this.labelTestStatus.TabIndex = 40;
            this.labelTestStatus.Text = "Test Status";
            // 
            // checkBoxTestStatus
            // 
            this.checkBoxTestStatus.AutoCheck = false;
            this.checkBoxTestStatus.AutoSize = true;
            this.checkBoxTestStatus.Checked = true;
            this.checkBoxTestStatus.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.checkBoxTestStatus.Enabled = false;
            this.checkBoxTestStatus.Location = new System.Drawing.Point(130, 134);
            this.checkBoxTestStatus.Name = "checkBoxTestStatus";
            this.checkBoxTestStatus.Size = new System.Drawing.Size(15, 14);
            this.checkBoxTestStatus.TabIndex = 39;
            this.checkBoxTestStatus.TabStop = false;
            this.checkBoxTestStatus.UseVisualStyleBackColor = true;
            // 
            // timerProgressBar
            // 
            this.timerProgressBar.Tick += new System.EventHandler(this.timerProgressBar_Tick);
            // 
            // toolTipDb
            // 
            this.toolTipDb.AutoPopDelay = 15000;
            this.toolTipDb.InitialDelay = 100;
            this.toolTipDb.ReshowDelay = 100;
            // 
            // DatabaseCredentialsUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.labelTestStatus);
            this.Controls.Add(this.checkBoxTestStatus);
            this.Controls.Add(this.textBoxStatusResult);
            this.Controls.Add(this.progressBarTestStatus);
            this.Controls.Add(this.labelDBSchema);
            this.Controls.Add(this.labelDBDataSource);
            this.Controls.Add(this.textBoxDBCatalogue);
            this.Controls.Add(this.textBoxDBDataSource);
            this.Controls.Add(this.textBoxDBPassword);
            this.Controls.Add(this.textBoxDBUsername);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.buttonTest);
            this.Name = "DatabaseCredentialsUserControl";
            this.Size = new System.Drawing.Size(350, 278);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonTest;
        private System.Windows.Forms.Label labelDBSchema;
        private System.Windows.Forms.Label labelDBDataSource;
        private System.Windows.Forms.TextBox textBoxDBCatalogue;
        private System.Windows.Forms.TextBox textBoxDBDataSource;
        private System.Windows.Forms.TextBox textBoxDBPassword;
        private System.Windows.Forms.TextBox textBoxDBUsername;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ProgressBar progressBarTestStatus;
        private System.Windows.Forms.TextBox textBoxStatusResult;
        private System.Windows.Forms.Label labelTestStatus;
        private System.Windows.Forms.CheckBox checkBoxTestStatus;
        private System.Windows.Forms.Timer timerProgressBar;
        private System.Windows.Forms.ToolTip toolTipDb;
    }
}
