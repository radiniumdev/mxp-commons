﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Data.SqlClient;
using System.IO;
using Cygnetic.MIPPluginCommons.Util;

namespace Cygnetic.MIPPluginCommons.Data
{
    public partial class DatabaseCredentialsUserControl : UserControl
    {

        public string Username
        {
            get
            {
                return textBoxDBUsername.Text;
            }

            set
            {
                textBoxDBUsername.Text = value;
            }
        }

        public string Password
        {
            get
            {

                return textBoxDBPassword.Text;
            }

            set
            {
                textBoxDBPassword.Text = value;
            }
        }

        public string DataSource
        {
            get
            {
                return textBoxDBDataSource.Text;
            }

            set
            {
                textBoxDBDataSource.Text = value;
            }
        }

        public string Catalogue
        {
            get
            {
                return textBoxDBCatalogue.Text;
            }

            set
            {
                textBoxDBCatalogue.Text = value;
            }
        }


        public DatabaseCredentialsUserControl()
        {
            InitializeComponent();
        }

        private void testConnection_Click(object sender, EventArgs e)
        {

            int timeout = 10000;
            labelTestStatus.Text = "Testing";
            textBoxStatusResult.Visible = true;
            textBoxStatusResult.Text = "Testing...";
            checkBoxTestStatus.CheckState = CheckState.Unchecked;
            labelTestStatus.ForeColor = Color.Orange;
            progressBarTestStatus.Visible = true;
            progressBarTestStatus.Minimum = 1;
            progressBarTestStatus.Maximum = timeout / timerProgressBar.Interval;
            progressBarTestStatus.Step = 1;
            progressBarTestStatus.Value = 1;
            buttonTest.Enabled = false;
            Refresh(); //redraw

            //Start testing
            timerProgressBar.Enabled = true;
            //Do in new thread so that progress bar updates
            ThreadPool.QueueUserWorkItem(new WaitCallback((object obj) =>
            {

                var task = Task.Run(() =>
                {
                    using (var conn = new SqlConnection(DatabaseUtility.SQL_CONNECTION_STRING(Username, DataSource, Catalogue, Password)))
                    {
                        SqlCommand command = new SqlCommand("SELECT 1;", conn);
                        if(command.Connection.State != ConnectionState.Open)
                        {
                            command.Connection.Open();
                        }

                        command.ExecuteNonQuery();
                    }

                });

                var timeoutTask = Task.Delay(timeout);

                //Make the taqsk continue and evaluate its exception if any to stop client from crashing (tasks run in the background)
                task.ContinueWith((x) =>
                {
                    if (task.Exception != null && !timeoutTask.IsCompleted)
                    {

                        labelTestStatus.Text = "Communication failed!";
                        textBoxStatusResult.Text += "Error trying to connect. Check the network path and credentials. \r\n" + task.Exception.Message;
                        checkBoxTestStatus.CheckState = CheckState.Unchecked;
                        labelTestStatus.ForeColor = Color.Red;

                    }
                });




                Task.WaitAny(task, timeoutTask);

                if (!timeoutTask.IsCompleted)
                {
                    if (task.IsCompleted && !task.IsFaulted)
                    {

                        labelTestStatus.Text = "Communication successful!";
                        textBoxStatusResult.Visible = false;
                        checkBoxTestStatus.CheckState = CheckState.Checked;
                        labelTestStatus.ForeColor = Color.Green;
                    }
                }
                else
                {

                    labelTestStatus.Text = "Communication failed!";
                    textBoxStatusResult.Text = "Connection timed out. Check that the Datasource, Catalogue and credentials are correct.";
                    checkBoxTestStatus.CheckState = CheckState.Unchecked;
                    labelTestStatus.ForeColor = Color.Red;
                }

                timerProgressBar.Enabled = false;
                progressBarTestStatus.Visible = false;
                buttonTest.Enabled = true;

            }));       
        }


        private void timerProgressBar_Tick(object sender, EventArgs e)
        {
            progressBarTestStatus.PerformStep();
            Refresh(); //redraw

        }
    }
}
