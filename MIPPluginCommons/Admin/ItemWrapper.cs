﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using VideoOS.Platform;

namespace Cygnetic.MIPPluginCommons.Admin
{
    public class ItemWrapper
    {
        public Item Item { get; set; }
        public static Dictionary<Type,Func<string, object>> converters = new Dictionary<Type, Func<string, object>>() {
            {typeof(int),  (value)=>{ return int.Parse(value); } },
            {typeof(string),  (value)=>{ return value; } },
            {typeof(bool),  (value)=>{ return bool.Parse(value); } },
            {typeof(Guid),  (value)=>{
                try{
                    return Guid.Parse(value);
                }
                catch(Exception e)
                {
                    return Guid.Empty;
                }
            }}
        };

        public Guid ID
        {
            get
            {
                return Item.FQID.ObjectId;
            }
        }

        public string Name
        {
            get { return Item.Name; }
            set { Item.Name = value; }
        }

        public ItemWrapper(Item item)
        {
            Item = item;
        }

        public override string ToString()
        {
            return Item.Name;
        }

        /**
         * Two wrapped items are the same if their Items' Object Ids are equal (We expect 2 Guids never to be the same)
         */
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (!(obj is ItemWrapper)) return false;

            ItemWrapper other = (ItemWrapper)obj;

            if (Item != null && other.Item == null) return false;
            if (Item == null && other.Item != null) return false;
            if (Item.FQID != null && other.Item.FQID == null) return false;
            if (Item.FQID == null && other.Item.FQID != null) return false;
            if (Item.FQID.ObjectId != other.Item.FQID.ObjectId) return false;

            return true;
        }

        /**
         * Two wrapped items are the same if their Items' Object Ids are equal (We expect 2 Guids never to be the same)
         */
        public override int GetHashCode()
        {
            int hashCode = Item.FQID.ObjectId.GetHashCode() * 17;
            hashCode += Item.GetType().GetHashCode() * 17;

            return hashCode;
        }



        protected K GetItemProperty<K>(string key, object defaultValue = null)
        {
            var func = converters[typeof(K)];

            if (func == null)
            {
                throw new NotSupportedException("Add an ItemWrapperConverter for the return type!");
            }

            if (Item.Properties.ContainsKey(key))
            {
                return (K)func.Invoke(Item.Properties[key]);
            }

            return (K)defaultValue;
        }

        protected void SetItemProperty(string key, string value)
        {
            if (Item.Properties.ContainsKey(key))
            {
                Item.Properties[key] = value;
            }
            else
            {
                Item.Properties.Add(key, value);
            }
        }
    }
}
