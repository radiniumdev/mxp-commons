﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoOS.Platform.Messaging;

/**
 * This class is to be used when wrapping MapResponseDataInstances as MapResponseDataInstance declares public fields instead of properties.
 * Primarily created for DataBinding compliance (like combobox)
 **/
namespace Cygnetic.MIPPluginCommons.Admin
{
    public class Map
    {

        public string Id { get; set; }
        public string DisplayName { get; set; }

        public Map(MapResponseDataInstance instance)
        {
            Id = instance.Id;
            DisplayName = instance.DisplayName;
        }

        public Map(string id, string displayName)
        {
            Id = id;
            DisplayName = displayName;
        }

        public static List<Map> Maps(MapResponseDataInstance[] mapInstances)
        {
            List<Map> maps = new List<Map>();

            foreach (MapResponseDataInstance mapInstance in mapInstances)
            {
                maps.Add(new Map(mapInstance));
            }

            return maps;
        }
    }
}
