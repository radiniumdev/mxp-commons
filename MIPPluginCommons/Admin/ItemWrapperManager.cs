﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoOS.Platform.Admin;
using VideoOS.Platform;

namespace Cygnetic.MIPPluginCommons.Admin
{
    public class ItemWrapperManager<K> : ItemManager where K : ItemWrapper
    {
        protected Guid _kind;
        protected Guid _pluginId;

        public ItemWrapperManager(Guid pluginId, Guid kind)
        {
            _kind = kind;
            _pluginId = pluginId;
        }

        public K CreateItem (Item parent)
        {
            FQID fqid = new FQID();
            fqid.Kind = _kind;
            fqid.ServerId = Configuration.Instance.ServerFQID.ServerId;
            fqid.ObjectId = Guid.NewGuid();
            if(parent != null)
            {
                fqid.ParentId = parent.FQID.ObjectId;
            }
            Item item = CreateItem(parent, fqid);
            K itemWrapper = (K)Activator.CreateInstance(typeof(K), new object[] { item });
            return itemWrapper;
        }

        public void SaveItem(K itemWrapper)
        {
            Configuration.Instance.SaveItemConfiguration(_pluginId, itemWrapper.Item);
        }

    }
}
