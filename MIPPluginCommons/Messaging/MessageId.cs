﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cygnetic.MIPPluginCommons.Messaging
{
    public class MessageId
    {
        public const string PluginLicenceChangedIndication = "CygneticTechnologies.PluginLicenceChangedIndication";
    }
}
