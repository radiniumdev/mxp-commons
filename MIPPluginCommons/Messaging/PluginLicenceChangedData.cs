﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cygnetic.MIPPluginCommons.Messaging
{
    public class PluginLicenceChangedData
    {
        public string PluginId { get; set; }
        public string Licence { get; set; }
    }
}
