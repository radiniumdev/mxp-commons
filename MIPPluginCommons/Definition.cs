﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoOS.Platform;

namespace Cygnetic.MIPPluginCommons
{
    public abstract class Definition : PluginDefinition
    {
        public class PluginOptions
        {
            public static readonly string Licences = "plugin_licences";
            public static readonly string LicenceExpiry = "plugin_licence_expiry";
        }

        static Definition()
        {
            
        }


        public Definition() : base()
        {
            
        }

        public static readonly string LicenceResourceClassPath = ".Resources.public.crt";
    }
}
